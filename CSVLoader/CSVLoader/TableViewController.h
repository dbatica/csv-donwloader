//
//  TableViewController.h
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>

@end
