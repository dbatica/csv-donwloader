//
//  ELMCsvParser.h
//  CSVLoader
//
//      Parser used to get CsvObjects array from string
//         Used regex @"(?:^|,)\"?((?<=\")[^\"]*|[^,\"]*)\"?(?=,|$)" - found it on stackoverflow :)
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ELMCsvParser : NSObject

/**
 *
 *  @param strCsvContent: the content of the csv file (all content)
 *  @return: an array of CsvObject
 */
- (NSArray*) parseCsvIntoModel:(NSString*)strCsvContent;

@end
