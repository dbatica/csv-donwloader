//
//  ELMCsvObjectsPool.h
//  CSVLoader
//
//      Stateless singletone used to provide data to the UI (delegate)
//      Delegate must be set befor any request are made (ELMCsvObjectsPoolDelegate)
//
//  Created by Daniel on 08/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CSVObject.h"

/**
 *  Protocol used to anounce the ui when a thumbnail is available
 */
@protocol ELMCsvObjectsPoolDelegate <NSObject>

//response handler for "startFetchingInputData" method
- (void) onRecordsAvailable:(NSArray*)csvObjects;

//response handler for "startFetchingThumbnailForObject" method - idx and object are the argument passed at request , if successful "object" will contain the thumbnail of the photo
- (void) onThumbnailIsAvailableForIndexPath:(NSIndexPath*)idx andCsvObject:(CSVObject*)object;

@end

@interface ELMCsvObjectsPool : NSObject

- (instancetype) init __attribute__((unavailable("init not available, call sharedInstance")));
+ (instancetype) sharedInstance;

- (void) setDelegate:(__weak id<ELMCsvObjectsPoolDelegate>)delegate;


- (void) startFetchingInputData;
- (BOOL) startFetchingThumbnailForObject:(CSVObject*)object withSize:(CGSize)newSize atTableIndexPath:(NSIndexPath*)idxPath;

//used by detail view controller
- (UIImage*) getThumbnailImageFromLocalPhoto:(CSVObject*)object withSize:(CGSize)newSize;

@end
