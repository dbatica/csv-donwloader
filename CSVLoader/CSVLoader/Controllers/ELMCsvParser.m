//
//  ELMCsvParser.m
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import "ELMCsvParser.h"
#import "CSVObject.h"

@implementation ELMCsvParser

- (NSArray*) parseCsvIntoModel:(NSString*)stRCsvContent
{
    //split into lines
    NSArray *csvLines=[stRCsvContent componentsSeparatedByString:@"\r\n"];
    
    if (csvLines.count) {
        NSMutableArray* csvObjects = [[NSMutableArray alloc] initWithCapacity:csvLines.count];
        
        for (NSString* line in csvLines) {
            [csvObjects addObject:[self getObjectFromLine:line]];
        }
        [csvObjects removeObjectAtIndex:0];//remove header
        return csvObjects;
    }
    
    return nil;
}

- (CSVObject*) getObjectFromLine:(NSString*)line
{
    NSString *pattern = @"(?:^|,)\"?((?<=\")[^\"]*|[^,\"]*)\"?(?=,|$)";
    NSRegularExpression *nameExpression = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                                    options:NSRegularExpressionAllowCommentsAndWhitespace
                                                                                      error:nil];
    NSArray *matches = [nameExpression matchesInString:line
                                               options:0
                                                 range:NSMakeRange(0, [line length])];
    NSString* strTitle = nil;
    NSString* strDescription = nil;
    NSString* strResourceUrl = nil;
    
    for (int i = 0; i < matches.count && i < 3; ++i) {
        NSTextCheckingResult *match = matches[i];
        NSRange matchRange = [match rangeAtIndex:1];
        NSString *matchString = [line substringWithRange:matchRange];
        
        if (0 == i) {
            strTitle = matchString;
        } else if (1 == i) {
            strDescription = matchString;
        } else if (2 == i) {
            strResourceUrl = matchString;
        }
    }
    
    return [[CSVObject alloc] initWithTitle:strTitle andDesc:strDescription andUrl:strResourceUrl];
}

@end
