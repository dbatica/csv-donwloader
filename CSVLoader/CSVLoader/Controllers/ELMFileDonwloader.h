//
//  ELMFileDonwloader.h
//  CSVLoader
//
//      Singletone used to download resources from a specific url and at completion
//      saves them localy to the designated location (most likely in Documents)
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ELMFileDonwloader : NSObject

- (instancetype) init __attribute__((unavailable("init not available, call sharedInstance")));
+ (instancetype) sharedInstance;


/**
 *      Downloads resource from "strUrl" (internet) and saves is to the file system at "strPath"
 *
 * @param strUrl: a valid url from where to download the resource
 * @param strPath: a valid url from the file system where to save the internet resource
 * @param handler: called when task is completed with success or no, 
                    contentType used to see what type of file is donwloaded (if request is done over http/https)
 *
 * @return: no - handler not called, yes - wait for handler
 */
- (BOOL) downloadResourceFromStrUrl:(NSString*)strUrl
                      andSaveToPath:(NSString*)strPath
                   andFinishHandler:(void (^)(BOOL success, NSString* contentType))handler;
@end
