//
//  ELMFileManager.m
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import "ELMFileManager.h"

static NSString* kStrCsvFileName = @"input.csv";

@interface ELMFileManager(){
    NSString* _mStrDirectoryPath;
}

@end

@implementation ELMFileManager

@synthesize csvFullPath = _csvFullPath;

- (id) init
{
    self = [super init];
    if (self) {
        //nothing yet;
        _csvFullPath = nil;
    }
    return self;
}

+ (instancetype) sharedInstance
{
    
    static ELMFileManager* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (NSString*) getDocumentsPath
{
    if (nil == _mStrDirectoryPath) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        _mStrDirectoryPath = paths[0];

    }
    
    return _mStrDirectoryPath;
}

- (NSString*) csvFullPath
{
    if (nil == _csvFullPath) {
        _csvFullPath = [[self getDocumentsPath] stringByAppendingPathComponent:kStrCsvFileName];
    }
    
    return _csvFullPath;
}

- (NSString*) getPathForPhotoName:(NSString*)photoName
{
    return [[self getDocumentsPath] stringByAppendingPathComponent:photoName];
}

- (BOOL) fileExistsAtPath:(NSString*)path
{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

- (void) removeFileAtPath:(NSString*)path
{
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

- (NSString*) getStringContentFromFile:(NSString*)strUrl
{
    NSError* err = nil;
    NSString* content = [[NSString alloc] initWithContentsOfFile:strUrl encoding:NSUTF8StringEncoding error:&err];
    
    return content;
}

- (void) clearTemporaryDirectory
{
    NSString *path = NSTemporaryDirectory();
    NSArray *array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
    for (NSString *string in array) {
        [[NSFileManager defaultManager] removeItemAtPath:[path stringByAppendingPathComponent:string] error:nil];
    }
}

#pragma mark - archive(save and retreive) local csvobjects

- (NSString*) getArchivePath
{
    return [[self getDocumentsPath] stringByAppendingPathComponent:CACHED_OBJECTS_ARCHIVE_NAME];
}

- (BOOL) hasArchiveForCsvObjects
{
    NSString* archivePath = [[self getDocumentsPath] stringByAppendingPathComponent:CACHED_OBJECTS_ARCHIVE_NAME];
    return [[NSFileManager defaultManager] fileExistsAtPath:archivePath];
}

- (void) archiveCsvObjectsArray:(NSArray*)csvObjects
{
    NSString* archivePath = [[self getDocumentsPath] stringByAppendingPathComponent:CACHED_OBJECTS_ARCHIVE_NAME];
    if ([[NSFileManager defaultManager] fileExistsAtPath:archivePath]) {
        [self removeFileAtPath:archivePath];
    }
    
    [NSKeyedArchiver archiveRootObject:csvObjects toFile:archivePath];
}

- (NSArray*) getCsvObjectsArrayFromArchive
{
    NSString* archivePath = [[self getDocumentsPath] stringByAppendingPathComponent:CACHED_OBJECTS_ARCHIVE_NAME];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:archivePath]) {
        NSArray* csvObjects = (NSArray*)[NSKeyedUnarchiver unarchiveObjectWithFile:archivePath];
        return csvObjects;
    }
    
    return nil;
}


@end
