//
//  ELMFileManager.h
//  CSVLoader
//
//      Singletone used for simple I/O operations + archiving and unarchiving of CsvObjects
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ELMFileManager : NSObject

@property (readonly) NSString* csvFullPath;

- (instancetype) init __attribute__((unavailable("init not available, call sharedInstance")));
+ (instancetype) sharedInstance;

//use only for small files (like the input file for ex
- (NSString*) getStringContentFromFile:(NSString*)strUrl;

//returns the full path of the photo (appends documents path with photo name - md5 of the resource url provided in the 3rd column of the csv file)
- (NSString*) getPathForPhotoName:(NSString*)photoName;

- (void) removeFileAtPath:(NSString*)path;
- (BOOL) fileExistsAtPath:(NSString*)path;

//clears /tmp folder of the app - the /tmp is filed 
- (void) clearTemporaryDirectory;

//cache csv objects in archive, use them if error at dowloading csv file
- (void) archiveCsvObjectsArray:(NSArray*)csvObjects;
- (NSArray*) getCsvObjectsArrayFromArchive;
- (BOOL) hasArchiveForCsvObjects;

@end
