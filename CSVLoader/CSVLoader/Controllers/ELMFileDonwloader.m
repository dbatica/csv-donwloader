//
//  ELMFileDonwloader.m
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import "ELMFileDonwloader.h"

@implementation ELMFileDonwloader

- (id) init
{
    self = [super init];
    if (self) {
        //nothing yet;
    }
    return self;
}

+ (instancetype) sharedInstance
{
    
    static ELMFileDonwloader* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (BOOL) downloadResourceFromStrUrl:(NSString*)strUrl
                      andSaveToPath:(NSString*)strPath
                   andFinishHandler:(void (^)(BOOL success, NSString* contentType))handler
{
    NSLog(@"ELMFileDonwloader - downloadResourceFromStrUrl: %@ andSaveToPath: %@", strUrl, strPath);
    
    NSURL* url = [NSURL URLWithString:strUrl];
    if (nil == url) {
        NSLog(@"ELMFileDonwloader - downloadResourceFromStrUrl: invalid source url");
        return NO;
    }
    
    NSURL* destUrl = [NSURL fileURLWithPath:strPath];
    if (nil == destUrl) {
        NSLog(@"ELMFileDonwloader - downloadResourceFromStrUrl: invalid destination url");
        return NO;
    }
    
    //delete old file at "strPath"
    NSError* err = nil;
    [[NSFileManager defaultManager] removeItemAtURL:destUrl error:&err];
    if (err) {
//        NSLog(@"ELMFileDonwloader - downloadResourceFromStrUrl - could not remove initial file");
    }
    
    NSURLSession* session = [NSURLSession sharedSession];
    NSURLRequest* request = [[NSURLRequest alloc] initWithURL:url];
    NSURLSessionDownloadTask *downloadTask = [session
                downloadTaskWithRequest:request
                completionHandler:^(NSURL* location,
                                    NSURLResponse* response,
                                    NSError* error) {
            
        if (nil == error) {
            
            NSString* contentType = nil;
            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                contentType = httpResponse.allHeaderFields[@"Content-Type"];
            }
            

            
            NSError* err = nil;
            [[NSFileManager defaultManager] moveItemAtURL:location toURL:destUrl error:&err];
            if (nil == err) {
                if (handler) { handler(YES, contentType); }
                
            } else {
                NSLog(@"ELMFileDonwloader - downloadResourceFromStrUrl: could not move file");
                if (handler) { handler(NO, contentType); }
            }
            [[NSFileManager defaultManager] removeItemAtURL:location error:&err];
            
            if (err) {
                NSLog(@"ELMFileDonwloader - downloadResourceFromStrUrl: could not remove temp file: %@", err);
            }
            
        } else {
//            NSLog(@"ELMFileDonwloader - downloadResourceFromStrUrl: err at dw: %@", error);
            if (handler) { handler(NO, nil); }
        }
                    
    }];
    
    if (nil != downloadTask) {
        [downloadTask resume];
        return YES;
    }

    return NO;
}

@end
