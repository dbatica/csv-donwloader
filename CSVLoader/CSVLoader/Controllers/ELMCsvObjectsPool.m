//
//  ELMCsvObjectsPool.m
//  CSVLoader
//
//  Created by Daniel on 08/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import "ELMCsvObjectsPool.h"
#import "ELMFileManager.h"
#import "ELMFileDonwloader.h"
#import "ELMCsvParser.h"
#import "UIImage+Thumbnail.h"


@interface ELMCsvObjectsPool() {
    NSArray* _mCsvObjectsArray;
    __weak id<ELMCsvObjectsPoolDelegate> _mDelegate;

    dispatch_queue_t _readQueue;
}

@property (atomic) NSMutableArray* mFetchingOperations;// could also limit the max number, and add future requests in another queue

@end

@implementation ELMCsvObjectsPool

- (id) init
{
    self = [super init];
    if (self) {
        _mCsvObjectsArray = nil;
        _mDelegate = nil;
        _readQueue = dispatch_queue_create("read_from_file_queue", NULL);
    }
    return self;
}

+ (instancetype) sharedInstance
{
    
    static ELMCsvObjectsPool* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void) setDelegate:(__weak id<ELMCsvObjectsPoolDelegate>)delegate
{
    _mDelegate = delegate;
    _mFetchingOperations = [[NSMutableArray alloc] init];
}

- (void) startFetchingInputData
{
    __weak __typeof(self) weakSelf = self;
    [self startLoadingCsvFileWithFinishHandler:^(BOOL success, NSArray *objects) {
        if (success) {
            [weakSelf onRecordsAvailable:objects];
        } else {
            NSLog(@"ELMCsvObjectsPool: records not available");
        }
    }];
}

- (void) startLoadingCsvFileWithFinishHandler:(void (^)(BOOL success, NSArray* objects))handler
{
    NSLog(@"ELMCsvObjectsPool:startLoadingCsvFileWithFinishHandler");
    [[ELMFileDonwloader sharedInstance] downloadResourceFromStrUrl:CSV_RESOURCE_LOCATION
                                                     andSaveToPath:CSV_LOCAL_PATH andFinishHandler:^(BOOL success, NSString* contentType) {
        
        if (success && [contentType containsString:@"csv"]) {
            NSLog(@"ELMCsvObjectsPool:startLoadingCsvFileWithFinishHandler received with success");
            ELMCsvParser* parser = [ELMCsvParser new];
            NSString* csvContent = [[ELMFileManager sharedInstance] getStringContentFromFile:CSV_LOCAL_PATH];
            NSArray* csvObjects = [parser parseCsvIntoModel:csvContent];
            csvContent = nil;
            

      
#ifdef DEBUG_MULTIPLE_OBJECTS
            NSMutableArray* multipliedArray = [[NSMutableArray alloc] initWithArray:csvObjects];
            for (int i = 0; i < 40; ++i) {
                [multipliedArray addObjectsFromArray:[csvObjects copy]];
            }
            _mCsvObjectsArray = multipliedArray;
#else
            _mCsvObjectsArray = csvObjects;
#endif
            [[ELMFileManager sharedInstance] archiveCsvObjectsArray:csvObjects];
      
            if (handler) { handler(YES, _mCsvObjectsArray); }
            
        } else {
            NSLog(@"ELMCsvObjectsPool:startLoadingCsvFileWithFinishHandler received with FAIL");
            if ([[ELMFileManager sharedInstance] hasArchiveForCsvObjects]) {
                //get cached csv objects
                NSLog(@"ELMCsvObjectsPool:startLoadingCsvFileWithFinishHandler using cached values");
                _mCsvObjectsArray = [[ELMFileManager sharedInstance] getCsvObjectsArrayFromArchive];
                if (handler) { handler(YES, _mCsvObjectsArray); }
            } else {
                if (handler) { handler(NO, nil); }//no cached objcests, the app will no nothing until restart with csv donwload with success
            }
        }
    }];
}

- (BOOL) startFetchingThumbnailForObject:(CSVObject*)object withSize:(CGSize)newSize atTableIndexPath:(NSIndexPath*)idxPath
{
    if ([_mFetchingOperations containsObject:idxPath]) {
        NSLog(@"startFetchingThumbnailForObject - already downloading for %@", idxPath);
        return YES;
    }
    
    __weak __typeof(self) weakSelf = self;
    [self.mFetchingOperations addObject:idxPath];
    return [self getThumbnailPhotoForCsvObject:object withSize:newSize withHandler:^(BOOL success, UIImage *image) {
        
        [[weakSelf mFetchingOperations] removeObject:idxPath];
        
        object.thumbnailImage = nil;
        if (success) {
            object.thumbnailImage = image;
            [weakSelf onThumbnailIsAvailableForIndexPath:idxPath andCsvObject:object];
        }
    }];
}

- (BOOL) getThumbnailPhotoForCsvObject:(CSVObject*)object withSize:(CGSize)newSize
                           withHandler:(void (^)(BOOL success, UIImage* image))handler
{
    __weak __typeof(self) weakSelf = self;
    
    NSLog(@"ELMCsvObjectsPool:getThumbnailPhotoForCsvObject for image this title: %@", object.strTitle);
    if (nil == object.strResourceUrlMD5) {
        NSLog(@"ELMCsvObjectsPool:getThumbnailPhotoForCsvObject for image this title: %@, no url for resource, FAIL", object.strTitle);
        if (handler) { handler(NO, nil); }
        return NO;
    }
    
    if (nil == object.strLocalPhotoPath) {
        object.strLocalPhotoPath = [[ELMFileManager sharedInstance] getPathForPhotoName:object.strResourceUrlMD5];
    }
    
    if ([[ELMFileManager sharedInstance] fileExistsAtPath:object.strLocalPhotoPath]) {
        NSLog(@"ELMCsvObjectsPool:getThumbnailPhotoForCsvObject - photo exists");
        
        dispatch_async(_readQueue, ^{
            UIImage* thumbnailPhoto = [weakSelf getThumbnailImageFromLocalPhoto:object withSize:newSize];
            if (handler) { handler (YES, thumbnailPhoto); }
        });
        
        return NO;
    }
    
    

    return [[ELMFileDonwloader sharedInstance] downloadResourceFromStrUrl:object.strResourceUrl
                                                     andSaveToPath:object.strLocalPhotoPath andFinishHandler:^(BOOL success, NSString* contentType) {

         if (success) {
             if ( [contentType containsString:@"image"] ) {
                 
                 UIImage* thumbnailPhoto = [self getThumbnailImageFromLocalPhoto:object withSize:newSize];
                 if (handler) { handler (YES, thumbnailPhoto); }
                 
             } else {
                 //delete tmp file
                 [[ELMFileManager sharedInstance] removeFileAtPath:object.strLocalPhotoPath];
                 object.strLocalPhotoPath = nil; // prevents any  future requests
                 object.strResourceUrl = nil;
                 object.strResourceUrlMD5 = nil;
                 if (handler) { handler (YES, nil); }
             }
         } else {
             if (handler) { handler (NO, nil); }
         }
     }];
}

- (UIImage*) getThumbnailImageFromLocalPhoto:(CSVObject*)object withSize:(CGSize)newSize
{
    UIImage* largeImage = [UIImage imageWithContentsOfFile:object.strLocalPhotoPath];
    UIImage* smallImage = [UIImage thumbnailFromImage:largeImage scaledToFillSize:newSize];
    
    return smallImage;

}

#pragma mark - ELMCsvObjectsPoolDelegate implementation

- (void) onRecordsAvailable:(NSArray*)csvObjects
{
    if (_mDelegate && [_mDelegate respondsToSelector:@selector(onRecordsAvailable:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_mDelegate onRecordsAvailable:csvObjects];
        });
    }
}

- (void) onThumbnailIsAvailableForIndexPath:(NSIndexPath*)idx andCsvObject:(CSVObject*)object
{
    if (_mDelegate && [_mDelegate respondsToSelector:@selector(onThumbnailIsAvailableForIndexPath:andCsvObject:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_mDelegate onThumbnailIsAvailableForIndexPath:idx andCsvObject:object];
        });
    }
}

@end
