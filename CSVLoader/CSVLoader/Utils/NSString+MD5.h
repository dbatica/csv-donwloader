//
//  NSString+MD5.h
//  CSVLoader
//
//          Category used to name urls from CSVObjects
//
//      Credit to: http://iosdevelopertips.com/core-services/create-md5-hash-from-nsstring-nsdata-or-file.html
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString*)MD5;

@end
