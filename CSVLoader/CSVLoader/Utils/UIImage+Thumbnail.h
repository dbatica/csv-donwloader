//
//  UIImage+Thumbnail.h
//  CSVLoader
//
// credits to : http://stackoverflow.com/questions/17884555/square-thumbnail-from-uiimagepickerviewcontroller-image
//  Created by Daniel on 08/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Thumbnail)

+ (UIImage *)thumbnailFromImage:(UIImage *)image scaledToFillSize:(CGSize)size;

@end
