//
//  TableViewController.m
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import "TableViewController.h"
#import "ImageWIthTitleAndDescCell.h"
#import "ViewController.h"
#import "ELMCsvObjectsPool.h"

@interface TableViewController () <ELMCsvObjectsPoolDelegate> {

}

@property (atomic) NSInteger mNrOfCsvObjs;
@property  (weak) NSArray* mCsvObjects;

@end

@implementation TableViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.mNrOfCsvObjs = 0;
    [[ELMCsvObjectsPool sharedInstance] setDelegate:self];
    [[ELMCsvObjectsPool sharedInstance] startFetchingInputData];

}

#pragma mark - ELMCsvObjectsPoolDelegate implementation

- (void) onRecordsAvailable:(NSArray*)csvObjects
{
    [self setMNrOfCsvObjs:csvObjects.count];
    [self setMCsvObjects:csvObjects];
    [self.tableView reloadData];
}

- (void) onThumbnailIsAvailableForIndexPath:(NSIndexPath*)idx andCsvObject:(CSVObject*)object
{
    NSLog(@"Received new photo for index path:%@", idx);
    [self.tableView reloadRowsAtIndexPaths:@[idx] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString* segueId = segue.identifier;
    if ([segueId isEqualToString:SEGUE_SHOW_DETAILS]) {//send csvObject to get details from
        ViewController* destination = (ViewController*)segue.destinationViewController;
        if ([sender isKindOfClass:[CSVObject class]]) {
            destination.csvObject = (CSVObject*)sender;
        }
    }
}

#pragma mark - UITableViewDelegate implementation
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    CSVObject* sender = self.mCsvObjects[indexPath.item];
    [self performSegueWithIdentifier:SEGUE_SHOW_DETAILS sender:sender];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kFImageWIthTitleAndDescCellRowHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (0 == self.mNrOfCsvObjs) {//to hide lines from beginning
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        return screenRect.size.height;
    }
    return 0;
}

#pragma mark - UITableViewDataSource implementation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.mNrOfCsvObjs;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *customCell = @"ImageWIthTitleAndDescCell";
    
    ImageWIthTitleAndDescCell* cell = (ImageWIthTitleAndDescCell*)[tableView dequeueReusableCellWithIdentifier:customCell];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ImageWIthTitleAndDescCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell.imageLoaderIndicator startAnimating];
    cell.titleLabel.text = @"N/A";
    cell.descriptionLabel.text = @"N/A";
    
    if (self.mCsvObjects) {
        
        CSVObject* csvForIndexPath = self.mCsvObjects[indexPath.item];
        cell.titleLabel.text = csvForIndexPath.strTitle;
        cell.descriptionLabel.text = csvForIndexPath.strDescription;
        
        if (nil != csvForIndexPath.thumbnailImage) {
            [cell.imageLoaderIndicator stopAnimating];
            cell.imageView.image = csvForIndexPath.thumbnailImage;
        } else {
            
//            if (indexPath.item == 2) {
            
                cell.imageView.image = nil;
                BOOL requestMade = [[ELMCsvObjectsPool sharedInstance] startFetchingThumbnailForObject:csvForIndexPath withSize:kThumbnailSize atTableIndexPath:indexPath];
                
                if (requestMade) {
                    [cell.imageLoaderIndicator startAnimating];
                } else {
                    [cell.imageLoaderIndicator stopAnimating];
                }
//            }
        }
    }
    


    
    return cell;
}




@end
