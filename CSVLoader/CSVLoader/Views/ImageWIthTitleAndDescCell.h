//
//  ImageWIthTitleAndDescCell.h
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <UIKit/UIKit.h>


static const CGFloat kFImageWIthTitleAndDescCellRowHeight = 78;

static const CGSize kThumbnailSize = {60, 60};

@interface ImageWIthTitleAndDescCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imageLoaderIndicator;

@end
