//
//  CSVObject.h
//  CSVLoader
//
//      Model object used to store info from the csv file
//      NSCoding protocol used to archive object to be future used if no internet or csv download fails
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CSVObject : NSObject <NSCoding>

//cached values
@property NSString* strTitle;
@property NSString* strDescription;
@property NSString* strResourceUrl;


@property NSString* strResourceUrlMD5;//property used as the file name where the photo is stored in Documents
@property NSString* strLocalPhotoPath;// filled by ELMCsvObjectsPool
@property UIImage* thumbnailImage;// filled by ELMCsvObjectsPool

- (instancetype) initWithTitle:(NSString*)title
                       andDesc:(NSString*)description andUrl:(NSString*)url;


@end
