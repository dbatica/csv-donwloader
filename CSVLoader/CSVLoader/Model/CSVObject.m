//
//  CSVObject.m
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import "CSVObject.h"
#import "NSString+MD5.h"

@implementation CSVObject

@synthesize strResourceUrlMD5 = _strResourceUrlMD5;

- (instancetype) initWithTitle:(NSString*)title
                       andDesc:(NSString*)description andUrl:(NSString*)url
{
    self = [super init];
    if (self) {
        if (title) { _strTitle = [[NSString alloc] initWithString:title]; }
        if (description) { _strDescription = [[NSString alloc] initWithString:description]; }
        if (url && url.length) {
            _strResourceUrl = [[NSString alloc] initWithString:url];
            _strResourceUrlMD5 = [[NSString alloc] initWithString:[_strResourceUrl MD5]];
        }
    }
    return self;
}

#pragma mark - NSCoding implementation

- (void) encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.strTitle forKey:@"strTitle"];
    [encoder encodeObject:self.strDescription forKey:@"strDescription"];
    [encoder encodeObject:self.strResourceUrl forKey:@"strResourceUrl"];
    [encoder encodeObject:self.strResourceUrlMD5 forKey:@"strResourceUrlMD5"];
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        _strTitle = [aDecoder decodeObjectForKey:@"strTitle"];
        _strDescription = [aDecoder decodeObjectForKey:@"strDescription"];
        _strResourceUrl = [aDecoder decodeObjectForKey:@"strResourceUrl"];
        _strResourceUrlMD5 = [aDecoder decodeObjectForKey:@"strResourceUrlMD5"];
    }
    
    return self;
}

@end
