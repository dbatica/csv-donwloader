//
//  ELMGlobalConstants.h
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#ifndef ELMGlobalConstants_h
#define ELMGlobalConstants_h

#define CSV_RESOURCE_LOCATION @"https://docs.google.com/spreadsheet/ccc?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE&single=true&gid=0&output=csv"

#define CSV_LOCAL_PATH ([ELMFileManager sharedInstance].csvFullPath)

#define SEGUE_SHOW_DETAILS @"segue_show_details"

#define CACHED_OBJECTS_ARCHIVE_NAME @"csv_objects.ar"

//for debug
//#define DEBUG_MULTIPLE_OBJECTS true


#endif /* ELMGlobalConstants_h */
