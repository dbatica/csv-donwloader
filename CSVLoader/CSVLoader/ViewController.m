//
//  ViewController.m
//  CSVLoader
//
//  Created by Daniel Batica on 07/12/15.
//  Copyright © 2015 Daniel Batica. All rights reserved.
//

#import "ViewController.h"
#import "ELMCsvObjectsPool.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *largeImageView;
@property (weak, nonatomic) IBOutlet UILabel *largeDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.csvObject) {
        self.largeDescriptionLabel.text = self.csvObject.strDescription;
        self.titleLabel.text = self.csvObject.strTitle;
        if (self.csvObject.thumbnailImage) {
            UIImage* largeImage = [UIImage imageWithContentsOfFile:self.csvObject.strLocalPhotoPath];
            self.largeImageView.image = largeImage;
            largeImage = nil;
        }
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
